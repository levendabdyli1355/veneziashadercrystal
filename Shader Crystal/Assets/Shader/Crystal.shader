Shader "Unlit/Crystal"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _Center("Center", Vector) = (0,0,0,0)
        _Color("Color", Color) = (1,1,1,1)
    }
        SubShader
        {
            Tags{ "RenderType" = "Opaque"}
            GrabPass{ "_BackgroundTexture"}
            
            LOD 100

            Pass
            {
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #pragma target 3.0

                #define MAX_STEPS 100
                #define MAX_DIST 10.
                #define SURF_DIST .001

                #include "UnityCG.cginc"

                struct appdata
                {
                    float4 vertex : POSITION;
                    float3 normal : NORMAL;
                };

                struct v2f
                {
                    float4 vertex : SV_POSITION;
                    float3 normal : NORMAL;
                };

                sampler2D _MainTex;
                float4 _MainTex_ST;

                sampler2D _ReflexTex;

                float3 _Center;

                float GetDist(float3 p)
                {
                    return distance(p, _Center);
                }

                v2f vert(appdata v)
                {
                    v2f o;
                    //o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
                    o.vertex = UnityObjectToClipPos(v.vertex); // vertex origin
                    o.normal = UnityObjectToWorldNormal(v.normal);
                    return o;
                }

                fixed4 frag(v2f i) : SV_Target
                {

                    float3 col = (1, 1, 1, 1);

                    return float4(col, 1.0);

                }
                ENDCG
            }
        }
}
