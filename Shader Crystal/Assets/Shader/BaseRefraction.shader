Shader "Unlit/BaseRefraction"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "Queue" = "Transparent"}
        GrabPass {}

        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 grabPos : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float3 screenNormal : TEXCOORD0; // fwd camera object renderer
                float4 screenPos : TEXCOORD1;
                float4 grabPos : TEXCOORD2;
            };

            sampler2D _GrabTexture; // call grabPass
            float4 _Color;

            v2f vert (appdata v)
            {
                v2f o;

                o.vertex = UnityObjectToClipPos(v.vertex);

                float4 tmp = ComputeScreenPos(o.vertex); // search

                float2 texCoord = tmp.xy / tmp.w;

                o.screenPos = float4(texCoord, 0, 1);

                o.screenNormal = normalize(UnityObjectToViewPos(v.normal)); // search

                o.grabPos = ComputeGrabScreenPos(o.vertex); // search

                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float2 refUV = i.screenPos.xy + i.screenNormal.xy * _Color.a;
    
                fixed4 col = tex2D(_GrabTexture, refUV) + _Color;

                return col;
            }
            ENDCG
        }
    }
}
