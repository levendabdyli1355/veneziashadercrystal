Shader "Basics/ViewSpaceNormals"
{
    Properties
    {
        _Color("Color", Color) = (1,1,1,1)
    }
        SubShader
    {
        Tags{ "RenderType" = "Opaque" "Queue" = "Transparent"}

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"

            float4 _Color;

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normals : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float3 normal : TEXCOORD0;
            };

            v2f vert(appdata v)
            {
                v2f o;

                //convert the vertex positions from object space to clip space so they can be rendered
                o.vertex = UnityObjectToClipPos(v.vertex);
                //o.normal = mul((float3x3)unity_ObjectToWorld, v.normals); //local space to world NSFW

                o.normal = normalize(UnityObjectToViewPos(v.normals)); //local space to view space
                //o.normal = normalize(mul((float3x3)UNITY_MATRIX_MV,v.normals)); //local space to view space NSFW

                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                return float4(i.normal, 1);
            }
            ENDCG
        }
    }
}
